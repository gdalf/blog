var gulp = require('gulp');
var del = require('del');
var minifySpace = require('./gulp-custom-plug/gulp-minify-space');

var outDir = 'views';

gulp.task('default', function() {
  gulp.src('src/*.*', { base: 'src' })
    .pipe(minifySpace())
    .pipe(gulp.dest(outDir));

  gulp.src('src/html/*.*', { base: 'src' })
    .pipe(minifySpace())
    .pipe(gulp.dest(outDir));

  gulp.src('src/css/*.*', { base: 'src' })
    .pipe(minifySpace())
    .pipe(gulp.dest(outDir));

  gulp.src('src/js/*.*', { base: 'src' })
    .pipe(minifySpace())
    .pipe(gulp.dest(outDir));

  gulp.src('src/img/*.*', { base: 'src' })
    .pipe(minifySpace())
    .pipe(gulp.dest(outDir));
});

gulp.task('clean', function(cb) {
  del([outDir], cb)
});