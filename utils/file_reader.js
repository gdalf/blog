const fs = require('fs');
const path = require('path');

const fileSystem = {};

fileSystem.tree = function tree(dir, node, filter) {

    node.type = 'root';
    node.name = 'root';
    node.node = [];

    if (fs.existsSync(dir)) {
        _tree(dir, node, filter);
    }
}

function _tree(dir, node, filter) {
    let fss = fs.readdirSync(dir);

    for (let i = 0; i < fss.length; i++) {

        let f = fss[i];

        if (filter && filter.indexOf(f) >= 0) {
            continue;
        }

        let absolutePath = path.join(dir, f);
        let stat = fs.lstatSync(absolutePath);

        if (stat.isDirectory()) {
            let newNode = new Node('dir', f, []);
            node.node.push(newNode);
            _tree(absolutePath, newNode);
        } else if (stat.isFile()) {
            f = path.basename(f, '.js');
            let newNode = new Node('file', f, absolutePath);
            node.node.push(newNode);
        }
    }
}

exports = module.exports = fileSystem;

class Node {
    constructor(type, name, node) {
        this.type = type;
        this.name = name;
        this.node = node;
    }
}