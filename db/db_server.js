const path = require('path');
const fs = require('fs');

/**
 * global dynamic database pool
 */
const pool = {};

const dbServer = {};

/**
 * 数据库根目录
 */
dbServer.DB_BASE = path.join(__dirname, 'database');

/**
 * 公共数据库路径
 */
dbServer.COMMON_DB = path.join(__dirname, 'database', 'common.db');

/**
 * 初始化数据库
 * 
 * @param {String[]} args 需要初始化的数据库列表
 */
dbServer.init = function init() {
  if (!fs.existsSync(this.DB_BASE)) {
    fs.mkdirSync(this.DB_BASE);
  }

  for (let i = 0; i < arguments.length; i++) {
    let arg = arguments[i];

    //TRY CREATE DB
    if (!fs.existsSync(arg)) {
      fs.writeFileSync(arg, '{}', 'utf8');
    }
  }
}

/**
 * 加载数据库到内存
 * 
 * @param {String[]} args 需要加载的数据库列表
 */
dbServer.dynamic = function dynamic() {
  for (let i = 0; i < arguments.length; i++) {
    let arg = arguments[i];

    if (pool[arg]) {
      continue;
    }

    let buf = fs.readFileSync(arg, 'utf8');

    if (!pool[arg]) {
      pool[arg] = new DB(arg, buf.toString());
    } else {
      pool[arg].data = buf.toString();
    }
  }
}

/**
 * 获取内存数据库对象
 */
dbServer.get = function get(dbname) {
  return pool[dbname];
}

/**
 * 持久化数据库对象
 * 
 * @param {String} dbname 数据名称
 * @param {DB} db 数据库操作对象
 * @param {*} callback 
 */
dbServer.persistence = function persistence(db, callback) {
  fs.writeFile(db.dbname, JSON.stringify(db.tree), 'utf8', callback);
}

/* DEMO BEGIN */
// function example() {
//   dbServer.init(dbServer.COMMON_DB);
//   dbServer.dynamic(dbServer.COMMON_DB, (db) => {
//     log.t(db.tree);

//     db.set('/user///admin/////', '1');
//     log.t(db.tree);

//     let user = JSON.parse('{"username":"admin", "token":"INDENSKJHWIQUYE12DE"}');
//     db.set('/user////admin', user);
//     log.t(db.tree);

//     dbServer.persistence(db, (err) => {
//       if (err) throw err;
//       log.t('persistence success');
//     });
//   });
// }
/* DEMO END */

/**
 * 数据库操作类
 */
class DB {

  constructor(dbname, data) {
    this.dbname = dbname;
    this.tree = JSON.parse(data);
  }

  toString() {
    return this.dbname + ' : ' + JSON.stringify(this.tree);
  }

  /**
   * 获取数据
   * 
   * @param {String} url 数据路径
   */
  get(url) {
    let urls = url.split('/');

    let val = this.tree;
    for (let i = 0; i < urls.length; i++) {

      let t = urls[i];
      if (!t) continue;

      val = val[t];

      if (!val) {
        return null;
      }
    }

    return val;
  }

  /**
   * 更改或添加数据
   * 
   * @param {String} url 数据路径
   * @param {Object} data 可以为String也可以为JsonObject
   */
  set(url, data) {
    let urls = url.split(/\/+/g);
    let val = this.tree;

    let end = urls.length;
    if (urls[end - 1] == '') {
      end--;
    }

    for (let i = 0; i < end - 1; i++) {
      let t = urls[i];
      if (!t) continue;

      if (!val[t]) {
        val[t] = {};
      }

      val = val[t];
    }

    val[urls[end - 1]] = data;
  }
}

/**
 * 初始化
 */
{
  dbServer.init(dbServer.COMMON_DB);
  dbServer.dynamic(dbServer.COMMON_DB);
}

exports = module.exports = dbServer;