const dbServer = require('../../db/db_server');
const ApiError = require('../error/ApiError');
const ApiErrorNames = require('../error/ApiErrorNames');

const dbCommon = dbServer.get(dbServer.COMMON_DB);

/* 初始化代码 */
{
  if (!dbCommon.get('/usrs/chengxi')) {
    dbCommon.set('/usrs/chengxi', { pwd: '339278353' });
    dbCommon.set('/usrs/chenqi', { pwd: '339278353' });
    dbCommon.set('/usrs/zl', { pwd: '4247488' });
  }
}

//获取用户
exports.getUser = async(ctx, next) => {
  ctx.body = {
    username: 'name',
    age: 30
  }
}

//用户注册
exports.registerUser = async(ctx, next) => {
  console.log('registerUser', ctx.request.body);
}

exports.login = async(ctx, next) => {

  let usr = dbCommon.get('/usrs/' + ctx.query.usr);

  if (usr) {

    if (usr.pwd !== ctx.query.pwd) {
      throw new ApiError(ApiErrorNames.USER_PWD_ERROR);
    }

    //TODO: token
    usr.token = ctx.query.usr + Date.now() + ctx.query.usr;
    dbCommon.set('/usrs/' + ctx.query.usr, usr);

    ctx.body = {
      name: ctx.query.usr,
      token: usr.token
    };
  } else {
    throw new ApiError(ApiErrorNames.USER_NOT_EXIST);
  }
}