const path = require('path');
const fs = require('fs');
const exec = require('child_process').exec;

const dbServer = require('../../db/db_server');
const fileReader = require('../../utils/file_reader');
const ApiError = require('../error/ApiError');
const ApiErrorNames = require('../error/ApiErrorNames');

const md = require('markdown-it')();

const dbCommon = dbServer.get(dbServer.COMMON_DB);

/* 初始化代码 */
{
  if (!fs.existsSync(path.join(__dirname, '..', '..', 'notes'))) {
    exec('mkdir notes', function(err, stdout, stderr) {
      if (err) {
        console.log('get weather api error:' + stderr);
      } else {
        console.log(stdout);
      }
    });
  }
}

exports.getTree = async(ctx, next) => {
  let usr = ctx.query.usr;

  if (!usr) throw new ApiError();

  let tree = {};
  fileReader.tree(path.join(__dirname, '..', '..', 'notes', usr), tree, '.git;.DS_Store');

  ctx.body = {
    tree: tree
  };

}

exports.note = async(ctx, next) => {
  let result = fs.readFileSync(ctx.query.path);
  result = md.render(result.toString());
  ctx.body = result;
}

exports.update = async(ctx, next) => {
  // https://gdalf@bitbucket.org/gdalf/notes.git

  let usr = ctx.query.usr;

  if (!usr) throw new ApiError();

  if (!fs.existsSync(path.join(__dirname, '..', '..', 'notes', usr))) {
    if (!ctx.query.gitpath) {
      throw new ApiError(ApiErrorNames.USER_NOTES_NOT_EXIST);
    }

    exec(`cd notes;git clone ${ctx.query.gitpath} ${usr}`, function(err, stdout, stderr) {
      if (err) {
        console.log('get weather api error:' + stderr);
      } else {
        console.log(stdout);
      }
    });

  } else {
    exec(`cd notes;cd ${usr};git pull`, function(err, stdout, stderr) {
      if (err) {
        console.log('get weather api error:' + stderr);
      } else {
        console.log(stdout);
      }
    });
  }

  ctx.body = '';
}