const router = require('koa-router')();
const note_controller = require('../../app/controllers/note_controller');

router.get('/getTree', note_controller.getTree);
router.get('/note', note_controller.note);
router.get('/update', note_controller.update);

module.exports = router;