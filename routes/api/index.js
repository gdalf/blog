const router = require('koa-router')();
const user_router = require('./user_router');
const note_router = require('./note_router');

router.use('/users', user_router.routes(), user_router.allowedMethods());
router.use('/notes', note_router.routes(), note_router.allowedMethods());

module.exports = router;