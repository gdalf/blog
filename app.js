const fs = require('fs');
const Koa = require('koa');
const path = require('path');
const http = require('http');
const https = require('https');
const serve = require('koa-static');
const logger = require('koa-logger');
const router = require('koa-router')();

const app = new Koa();
const api = require('./routes/api');
const auth_filter = require('./middlewares/auth_filter');
const response_formatter = require('./middlewares/response_formatter');

router.use('/api', api.routes(), api.allowedMethods());

app.use(logger());
app.use(auth_filter('^/html', '/login.html'));
app.use(serve(path.join(__dirname, 'views')));
app.use(response_formatter('^/api'));
app.use(router.routes(), router.allowedMethods());

// app.listen(80, () => {
//   console.log('listening on port 3000');
// });

const options = {
  key: fs.readFileSync('ssl/214316192800927.key'),
  cert: fs.readFileSync('ssl/214316192800927.pem')
};
http.createServer(function(req, res) {
  res.writeHead(301, {'Location': 'https://www.graydalf.com'});
  res.end();
}).listen(80);
https.createServer(options, app.callback()).listen(443);