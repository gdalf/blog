const dbServer = require('../db/db_server');

const dbCommon = dbServer.get(dbServer.COMMON_DB);

const auth_filter = (pattern, redirectUrl) => {

  let reg = new RegExp(pattern);

  let checkUserPermission = function(ctx) {
    let cookieUsr = ctx.cookies.get('usr');

    try {
      cookieUsr = JSON.parse(cookieUsr);
    } catch (e) {
      return false;
    }

    let usr = dbCommon.get('/usrs/' + cookieUsr.name);
    if (usr && usr.token === cookieUsr.token) {
      return true;
    } else {
      return false;
    }
  };

  return async(ctx, next) => {

    if (reg.test(ctx.originalUrl) && !checkUserPermission(ctx)) {
      ctx.redirect(redirectUrl);
    } else {
      await next();
    }
  }
}



module.exports = auth_filter;