(function() {

  var LOCALSTORAGE_USR_KEY = 'usr';

  /**
   * aside doc-tree hide or show
   */
  document.querySelector('#submit').addEventListener('click', function() {
    var usr = document.querySelector('#usr').value;
    var pwd = document.querySelector('#pwd').value;

    if (usr && pwd) {
      $.ajax({
        url: '/api/users/login',
        data: {
          usr: usr,
          pwd: pwd
        },
        success: function(res) {
          if (res.code === 0) {
            var bean = JSON.stringify(res.data);
            common.cookie.setCookie('usr', bean, 30);
            window.location.href = './html/blog.html';
          } else {
            // { code: 101, message: '用户不存在' });
            // { code: 102, message: '密码错误' });
            var usr = document.querySelector('#usr');
            var pwd = document.querySelector('#pwd');
            usr.parentNode.className = '';
            pwd.parentNode.className = '';
            if (res.code === 101) {
              usr.parentNode.className = 'error';
              usr.value = '';
            } else if (res.code === 102) {
              pwd.parentNode.className = 'error';
              pwd.value = '';
            }
          }
        },
        dataType: 'json'
      });
    }
  })

}())