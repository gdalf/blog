var common = function() {

  var cookie = {
    setCookie: function(c_name, value, expiredays) {

      var exdate = new Date();

      exdate.setDate(exdate.getDate() + expiredays)

      document.cookie = c_name + "=" + value + ";path=/" + ((expiredays == null) ? "" : ";expires=" + exdate.toGMTString())
    },

    getCookie: function(c_name) {
      if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
          c_start = c_start + c_name.length + 1;
          c_end = document.cookie.indexOf(";", c_start);
          if (c_end == -1) c_end = document.cookie.length;
          return unescape(document.cookie.substring(c_start, c_end));
        }
      }
      return "";
    },

    removeCookie: function(name) {
      var exp = new Date();
      exp.setTime(exp.getTime() - 1);
      var cval = this.getCookie(name);
      if (cval != null)
        document.cookie = name + "=" + cval + ";path=/" + ";expires=" + exp.toGMTString();
    }
  };

  var dialog = {
    template: '<div class="mark-box"><div class="content-box"><h1>This is title</h1><input type="text" placeholder="https://cychengxi:xxxxx@bitbucket.org/gdalf/notes_xi.git"><input type="button" value="submit"></div></div>',
    show: function(title, placeholder, onClick) {
      var $doc = $(this.template);
      $doc.find('h1').html(title);
      $doc.find('input[type="text"]').attr("placeholder", placeholder);

      $('body').append($doc);

      $doc.click(function() {
        $doc.remove();
      });

      $doc.find('input[type="button"]').click(function() {
        var val = $doc.find('input[type="text"]').val();
        onClick(val);
        $doc.remove();
      });

      $doc.find('.content-box').click(function(e) {
        e.stopPropagation();
      });
    }
  };

  var loading = {
    template: '<div class="mark-box"></div>',
    show: function(time) {
      var $doc = $(this.template);
      var $boxLoading = $("<div class='boxLoading'></div>");
      $doc.append($boxLoading);
      $('body').append($doc);

      if (time && isFinite(time)) {
        setTimeout(function() {
          $doc.remove();
        }, time);
      } else {
        return {
          hide: function() {
            $doc.remove();
          }
        };
      }
    }
  };

  return {
    cookie: cookie,
    dialog: dialog,
    loading: loading
  }
}();