(function() {

  var loadTree, tmpDOM, i, usr, init, buildTree, isLoaded, updateGitRegistory;

  /**
   * usr
   */
  usr = common.cookie.getCookie('usr');
  usr = JSON.parse(usr);

  /**
   * nav top
   */
  document.querySelector('#usr').innerHTML = usr.name;

  document.querySelector('#out').addEventListener('click', function() {
    common.cookie.removeCookie('usr');
    window.location.href = '../login.html';
  });

  document.querySelector('#update').addEventListener('click', function() {
    var crl = common.loading.show();

    $.ajax({
      url: '/api/notes/update',
      data: {
        usr: usr.name
      },
      dataType: 'json',
      success: function(res) {
        if (res.code === 0) {
          isLoaded = false;
          setTimeout(function() {
            loadTree();
            crl.hide();
          }, 5000);
        } else {
          crl.hide();
          updateGitRegistory();
        }
      },
      error: function() {
        crl.hide();
      }
    });
  });

  /**
   * aside doc-tree hide or show
   */
  document.querySelector('main>aside>.doc-tree-taggle').addEventListener('click', function() {
    var asideDOM = document.querySelector('main>aside');
    if (asideDOM.className.includes('drawer')) {
      asideDOM.className = asideDOM.className.replace(/drawer/, '').trim();
    } else {
      asideDOM.className += ' drawer';
    }
  });

  updateGitRegistory = function() {

    common.dialog.show('git registory', 'https://usr:pwd@bitbucket.org/usr/notes.git', function(data) {

      var crl = common.loading.show();

      $.ajax({
        url: '/api/notes/update',
        data: {
          gitpath: data
        },
        dataType: 'json',
        success: function(res) {
          if (res.code === 0) {
            setTimeout(function() {
              isLoaded = false;
              loadTree();
              crl.hide();
            }, 5000);
          }
        },
        error: function() {
          crl.hide();
        }
      });
    });
  };

  loadTree = function() {
    $.ajax({
      url: '/api/notes/getTree',
      data: {
        usr: usr.name
      },
      success: function(res) {
        if (res.code === 0) {
          initTree(res.data.tree);
        }
      },
      dataType: 'json'
    });
  };

  initTree = function(data) {

    $('.doc-tree').html('');
    $('.doc-tree').append($("<h2>Document tree</h2>"));
    buildTree(data, $('.doc-tree'));

    /**
     * aside doc-tree click event
     */
    tmpDOM = document.querySelectorAll('main>aside>.doc-tree span');
    for (i = 0; i < tmpDOM.length; i++) {
      tmpDOM[i].addEventListener('click', function(item) {
        var ulDOM = item.target.nextSibling;
        if (ulDOM.hasAttribute('hidden')) {
          ulDOM.removeAttribute('hidden');
        } else {
          ulDOM.setAttribute('hidden', 'hidden');
        }
      });
    }
  };

  buildTree = function(tree, $doc) {

    var $ul = $("<ul></ul>");

    $doc.append($ul);

    for (var key in tree.node) {
      var node = tree.node[key];
      var $li = $("<li></li>");
      $ul.append($li);
      if (node.type == 'dir') {
        var $leaf = $(`<span>${node.name}</span>`);
        $li.append($leaf);
        buildTree(node, $li);
      } else if (node.type == 'file') {
        var path = node.node.replace(/\\/g, '\\\\');
        var $leaf = $(`<a href="#" onclick="load('${path}')"> ${node.name}</a>`);
        $li.append($leaf);

        if (!isLoaded && path.endsWith('.md')) {
          isLoaded = true;
          load(path);
        }
      }
    }
  }

  //first load
  loadTree();
}());

function load(arg) {
  $.ajax({
    url: "/api/notes/note",
    dataType: 'json',
    data: {
      path: arg
    },
    success: function(result) {
      $('main>article>section').html(result.data);
    },
    error: function(err) {
      console.log(JSON.stringify(err));
    }
  });
}